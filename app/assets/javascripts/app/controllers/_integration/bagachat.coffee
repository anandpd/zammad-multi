class Index extends App.ControllerIntegrationBase
  featureIntegration: 'bagachat_integration'
  featureName: 'Bag A Chat'
  featureConfig: 'bagachat_config'
  description: [
    ['This service sends Public Reply to your %s account.', 'Bag A Chat']
  ]

  render: =>
    super
    new App.SettingsForm(
      area: 'Integration::BagAChat'
      el: @$('.js-form')
    )

class State
  @current: ->
    App.Setting.get('bagachat_integration')

App.Config.set(
  'IntegrationBagAChat'
  {
    name: 'Bag A Chat'
    target: '#system/integration/bagachat'
    description: 'Bag A Chat Integration to send Reply.'
    controller: Index
    state: State
  }
  'NavBarIntegrations'
)
