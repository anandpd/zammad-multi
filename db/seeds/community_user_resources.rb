org_community = Organization.create_if_not_exists(
  id: 1,
  name: 'Zammad Foundation',
)
user_community = User.create_or_update(
  id: 2,
  login: 'info@bagachat.com',
  firstname: 'Bag A',
  lastname: 'Chat',
  email: 'info@bagachat.com',
  password: 'tech2016',
  active: true,
  roles: [ Role.find_by(name: 'Customer') ],
  organization_id: org_community.id,
)

UserInfo.current_user_id = user_community.id

ticket = Ticket.create!(
  group_id: Group.find_by(name: 'Users').id,
  customer_id: User.find_by(login: 'info@bagachat.com').id,
  title: 'Welcome to Zammad!',
)
Ticket::Article.create!(
  ticket_id: ticket.id,
  type_id: Ticket::Article::Type.find_by(name: 'phone').id,
  sender_id: Ticket::Article::Sender.find_by(name: 'Customer').id,
  from: 'Zammad Feedback <feedback@zammad.org>',
  body: 'Welcome!

Thank you for choosing Zammad.

You will find updates and patches at https://zammad.org/. Online
documentation is available at https://zammad.org/documentation. Get
involved (discussions, contributing, ...) at https://zammad.org/participate.

Regards,

Your Zammad Team
',
  internal: false,
)

UserInfo.current_user_id = 1
