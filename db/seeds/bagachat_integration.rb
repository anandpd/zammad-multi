
# #### Bag A Chat Integration Setting
Setting.create_if_not_exists(
  title: 'BagAChat integration',
  name: 'bagachat_integration',
  area: 'Integration::Switch',
  description: 'Defines if Bag A Chat (https://link.bagachat.com) is enabled or not.',
  options: {
    form: [
      {
        display: '',
        null: true,
        name: 'bagachat_integration',
        tag: 'boolean',
        options: {
          true  => 'yes',
          false => 'no',
        },
      },
    ],
  },
  state: true,
  preferences: {
    prio: 1,
    permission: ['admin.integration'],
  },
  frontend: false
)


Setting.create_if_not_exists(
  title: 'API Token',
  name: 'bagachat_apitoken',
  area: 'Integration::BagAChat',
  description: 'Defines the API Token of BagAChat to send reply.',
  options: {
    form: [
      {
        display: '',
        null: false,
        name: 'bagachat_apitoken',
        tag: 'input',
        placeholder: 'Your API Token Here',
      },
    ],
  },
  state: ENV['BAGACHAT_API_TOKEN'],
  preferences: {
    prio: 2,
    permission: ['admin.integration'],
  },
  frontend: false,
)
Setting.create_if_not_exists(
  title: 'WebHook',
  name: 'bagachat_webhook',
  area: 'Integration::BagAChat',
  description: 'Defines the Endpoint to send reply through BagAChat.',
  options: {
    form: [
      {
        display: '',
        null: false,
        name: 'bagachat_webhook',
        tag: 'input',
        placeholder: 'Your WebHook Endpoint here',
      },
    ],
  },
  state: 'https://link.bagachat.com/sendresponse.bg',
  preferences: {
    prio: 2,
    permission: ['admin.integration'],
  },
  frontend: false,
)

#Set BagAChat API Token
setting = Setting.find_by(name: 'bagachat_apitoken')
setting.state_current['value'] = ENV['BAGACHAT_API_TOKEN']
setting.save!

#Set Zammad Num Agents
setting = Setting.find_by(name: 'system_agent_limit')
setting.state_current['value'] = 5
setting.save!

#Set Zammad First User
    #Set Zammad First User Password
    admin = User.create_or_update(
      login: ENV['ZAMMAD_USER'],
      firstname: 'Admin',
      lastname: "Admin",
      email: ENV['ZAMMAD_USER'],
      password: ENV['ZAMMAD_PASS'],
      active: true,
      roles: Role.where(name: %w(Admin Agent)),
      updated_by_id: 1,
      created_by_id: 1,
    )
    
    existingToken = Token.check(action: 'ticket.agent', name: ENV['BAGACHAT_API_TOKEN'])
    if !existingToken
        #Set Zammad Token
        Token.create(
          user_id: admin.id,
          persistent: true,
          name: ENV['BAGACHAT_API_TOKEN'],
          action: 'api',
          label: 'ForBagAChat_DoNotDelete',
          preferences: {
            permission: ['admin.tag', 'ticket.agent']
          },
        )
     end

#Set System Init is Done
setting = Setting.find_by(name: 'system_init_done')
setting.state_current['value'] = true
setting.save!

#Set FQDN
setting = Setting.find_by(name: 'fqdn')
setting.state_current['value'] = ENV['USER_FQDN']
setting.save!
